pub mod dl;
pub mod pool;

// TODO: Support JP endpoint
pub const URI_AUTHORIZE: &str = "https://play.dlsite.com/eng/api/authorize";
pub const URI_PURCHASES: &str = "https://play.dlsite.com/eng/api/purchases"; // ?page=2
pub const URI_DOWNLOAD:  &str = "https://play.dlsite.com/eng/api/download"; // ?workno=RE204729

// TODO: ziptree+indexeddb download/sync support
pub const URI_DLTOKEN:   &str = "https://play.dlsite.com/eng/api/download_token"; // ?workno=RE214305
// https://play.dl.dlsite.com/content/work/doujin/RJ215000/RJ214305/ziptree.json?token=1609643246_c512778fe56a2f950eeffeb5c5af6bcf031a0354&expiration=1609643246
