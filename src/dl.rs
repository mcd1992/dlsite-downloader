use std::error::Error;
use std::boxed::Box;
use std::fs::File;
use std::path::Path;
use std::io::prelude::*;

#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};

use futures::StreamExt;
use regex::Regex;

pub async fn follow_redirects(clap: &clap::ArgMatches<'_>, client: &reqwest::Client, url: reqwest::Url) -> Result<reqwest::Response, Box<dyn Error>> {
    let mut depth: u64 = 0;
    let mut url = url.clone();
    let mut cookiejar = cookie::CookieJar::new();
    cookiejar.add(cookie::Cookie::new("__DLsite_SID", clap.value_of("sid").unwrap().clone().to_owned()));

    let response: reqwest::Response = loop {
        debug!("follow_redirects requesting: {}", url);
        //cookiejar.add(cookie::Cookie::new("depth", depth.to_string()));
        let mut req = client.request(reqwest::Method::GET, url);
        let mut cookies_str = String::new();
        for c in cookiejar.iter() {
            cookies_str.push_str(format!("{}; ", &c.to_string()).as_str());
        }
        req = req.header(reqwest::header::COOKIE, cookies_str);

        let res = req.send().await?;
        for c in res.cookies() {
            trace!("follow_redirects inserted set-cookie: {}={}", c.name(), c.value());
            cookiejar.add(cookie::Cookie::new(c.name().to_string(), c.value().to_string()));
        }

        if res.status().is_redirection() {
            if depth > 4 {
                warn!("follow_redirects overflow, breaking out at {} redirects", depth);
                break res;
            };
            url = reqwest::Url::parse(res.headers().get("location").unwrap().to_str().unwrap()).unwrap();
            depth += 1;
            continue;
        }

        debug!("follow_redirects returning response: {:#?}", res);
        break res;
    };

    Ok(response)
}

// Download the archive into $outputdir/archives/RJ000000/RJ000000.part1.exe .part2.rar ect.
// DLsite splits zips that are bigger than 700MB into rar files with a self extracting exe
pub async fn download_archives(clap: &clap::ArgMatches<'_>, client: &reqwest::Client, works: Vec<String>) -> Result<(), Box<dyn Error>> {
    //let mut download_urls: HashMap<String, Path> = HashMap::new();
    for work_id in works {
        debug!("downloading {}", work_id);
        let url = reqwest::Url::parse_with_params(crate::URI_DOWNLOAD, &[("workno", work_id.clone())])?;
        let mut res = follow_redirects(&clap, &client, url).await?;

        if !res.status().is_success() {
            warn!("failed to download archives for {} - {:#?}", work_id, res);
            continue;
        }

        if res.url().as_str().contains("download/split") { // multipart rar
            let content_type = res.headers().get("content-type").unwrap_or(&reqwest::header::HeaderValue::from_static("")).to_str().unwrap_or("").to_owned();
            if content_type.contains("text/html") {
                info!("found split archive: {}", work_id);
                let btn_dl_selector = scraper::Selector::parse(".btn_dl").unwrap();
                let html = scraper::Html::parse_document(&res.text().await?.clone());
                for dl_link in html.select(&btn_dl_selector) {
                    let dl_link = dl_link.value().attr("href").unwrap();
                    res = follow_redirects(&clap, &client, reqwest::Url::parse(dl_link)?).await?;
                    debug!("split dl archive found at: {}", res.url().as_str());
                }
                continue;
            }
        } else {

        }

        // Download loop
        let mut header_filename = String::from(res.headers().get("content-disposition").unwrap().to_str().unwrap());
        let filename_regex = Regex::new(r#".*filename="(.*?)""#)?;
        header_filename = filename_regex.captures(&header_filename).unwrap().get(1).unwrap().as_str().to_string();

        let file_path_str = format!("{}/archives/{}/{}",
            clap.value_of("outputdir").unwrap().to_string(),
            work_id.clone(),
            header_filename,
        );

        let file_path = Path::new(&file_path_str);
        std::fs::create_dir_all(file_path.parent().unwrap())?;

        let mut file_time = filetime::FileTime::now();
        if res.headers().get("last-modified").is_some() {
            let mod_time_str = res.headers().get("last-modified").unwrap().to_str()?;
            debug!("last-modified for {}: {}", file_path_str, mod_time_str);
            let mut mod_epoch = chrono::DateTime::parse_from_rfc2822(mod_time_str); // https://tools.ietf.org/html/rfc7232#section-2.2
            if mod_epoch.is_err() {
                // apparently dlsite ignores rfc7232 and just uses rfc3339 ?
                mod_epoch = chrono::DateTime::parse_from_rfc3339(mod_time_str);
            }
            if mod_epoch.is_ok() {
                let e = mod_epoch.unwrap();
                file_time = filetime::FileTime::from_unix_time(e.timestamp(), e.timestamp_subsec_nanos());
                debug!("file_time: {:#?}", file_time);
            }
        }

        let mut cursor = 0;
        { // new scope to drop file and stream when completed
            let mut file = File::create(file_path)?;
            let mut stream = res.bytes_stream();
            while let Some(data) = stream.next().await {
                let data = data?;
                file.write_all(&data)?;
                cursor += data.len();
            }
        }
        filetime::set_file_mtime(file_path, file_time)?;
        debug!("wrote {} bytes to {}", cursor, file_path_str);
    }

    Ok(())
}
