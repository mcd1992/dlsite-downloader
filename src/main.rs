use std::error::Error;
use std::boxed::Box;
use std::fs::File;
use std::path::Path;
use std::io::prelude::*;

#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};

use dlsite_downloader::*;
use clap::{App, Arg};
use regex::Regex;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("DLSite Downloader")
    .version(clap::crate_version!())
    .author(clap::crate_authors!())
    .about(clap::crate_description!())
    .setting(clap::AppSettings::ArgRequiredElseHelp)
    // Flags
    .arg(
        Arg::with_name("overwrite")
            .short("o")
            .long("overwrite")
            .takes_value(false)
            .help("Overwrite any existing files"),
    )
    .arg(
        Arg::with_name("verbose")
            .short("v")
            .long("verbose")
            .multiple(true)
            .help("Sets the level of verbosity. Use multiple v's for more verbose output\n[1=INFO, 2=DEBUG, 3=TRACE, 4=SILLY]"),
    )

    // Options
    .arg(
        Arg::with_name("ids")
            .short("i")
            .long("ids")
            .value_name("IDS")
            .help("Work IDs or range to download"),
    )
    .arg(
        Arg::with_name("parallel")
            .short("p")
            .long("parallel")
            .value_name("PARALLEL_DOWNLOADS")
            .help("Max number of parallel downloads to have at once"),
    )
    .arg(
        Arg::with_name("sid")
            .short("s")
            .long("sid")
            .value_name("SID")
            .required(true)
            .help("DLsite_SID cookie value for authentication"),
    )

    // Arguments
    .arg(
        Arg::with_name("outputdir")
            .value_name("OUTPUT_DIR")
            .index(1)
            .required(true)
            .help("Target directory to output the downloads into"),
    )
    .get_matches();

    // TODO: .module can't really be called with a 'default value' that keeps the Vec<String> empty...
    //let full_verbose = matches.occurrences_of("verbose") > 3;
    stderrlog::new()
        .quiet(false)
        .verbosity(matches.occurrences_of("verbose") as usize + 1)
        .timestamp(stderrlog::Timestamp::Off)
        .module(module_path!()) // Ignore crates using `log`
        //.show_module_names(full_verbose)
        .init()?;

    // Check SID length and regex
    let sid_regex = Regex::new(r#"[a-z0-9]{26,}"#)?;
    let sid_string = matches.value_of("sid").unwrap();
    if !sid_regex.is_match(sid_string) {
        panic!("Invalid SID value. Should be alphanumeric with at least 26 characters. See __DLsite_SID cookie in your browser.")
    }

    // Check if outputdir exists, create otherwise
    let works_path = Path::new(matches.value_of("outputdir").unwrap());
    std::fs::create_dir_all(works_path)?;

    // Create reqwest client with defaults
    let cookie_string = format!("__DLsite_SID={}", sid_string);
    let mut headers = reqwest::header::HeaderMap::new();
    headers.insert(reqwest::header::COOKIE, reqwest::header::HeaderValue::from_str(&cookie_string).unwrap());

    let client = reqwest::Client::builder()
        .proxy(reqwest::Proxy::all("socks5://127.0.0.1:8080")?) // TODO: allow command line proxy setting
        .user_agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
        .cookie_store(true)
        .default_headers(headers)
        .redirect(reqwest::redirect::Policy::none()) // handle redirects manually until remove_sensitive_headers follows rfc6265
        .build()?;

    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name("dlsite-downloader")
        //.worker_threads(1) // TODO: allow overriding default of n-cores
        .on_thread_start(|| {
            trace!("tokio on_thread_start");
        })
        .on_thread_stop(|| {
            trace!("tokio on_thread_stop");
        })
        .build()?;
    rt.block_on(async move {
        let _res = tokio_runner(matches.clone(), client).await;
    });

    trace!("exiting main");
    Ok(())
}

async fn tokio_runner(matches: clap::ArgMatches<'_>, mut client: reqwest::Client) -> Result<(), Box<dyn Error>> {
    // Create client and generate cookies from authorization endpoint
    let response = client.get(URI_AUTHORIZE).send().await?;
    assert_eq!(response.status(), reqwest::StatusCode::OK);
    let data = response.text().await?;
    debug!("authorization response: {:#?}", data);
    assert!(data != "[]", "Failed to authenticate; SID should be alphanumeric with at least 26 characters. See __DLsite_SID cookie in your browser.");

    // Get available works from purchases endpoint
    let works_json = query_purchases(&matches, &mut client).await?;
    debug!("works_json len: {}", works_json.len());
    //trace!("works_json: {:#?}", works_json);

    // Start downloading all purchased archives
    crate::dl::download_archives(&matches, &client, vec![
        String::from("RE294140"), // 894KB zip
        //String::from("RE284724"), // 30MB zip
        //String::from("RE293189"), // 163MB zip
        String::from("RE204729"), // multi rar
    ]).await?;

    Ok(())
}

async fn query_purchases(clap: &clap::ArgMatches<'_>, client: &reqwest::Client) -> Result<Vec<String>, Box<dyn Error>> {
    let mut cur_pagenum = 1;
    let mut works: Vec<String> = vec![];
    let mut works_json: Vec<serde_json::Value> = vec![];
    loop {
        let url = reqwest::Url::parse_with_params(URI_PURCHASES, &[("page", cur_pagenum.to_string())])?;
        let response = client.get(url).send().await?;
        let data = response.text().await?;
        let json: serde_json::Value = serde_json::from_str(&data)?;
        let num_works = json["total"].as_f64().unwrap();
        let page_limit = json["limit"].as_f64().unwrap();
        let max_pagenum = (num_works / page_limit).ceil() as u64;
        let work_objs = json["works"].as_array().unwrap();
        for o in work_objs.iter() {
            works.push(o["workno"].as_str().unwrap().to_string());
            works_json.push(o.to_owned());
        }
        if cur_pagenum >= max_pagenum { break; }
        cur_pagenum += 1;
    }

    // Save works.json
    let works_pathstr = format!("{}/works.json", clap.value_of("outputdir").unwrap().to_string());
    let works_path = Path::new(&works_pathstr);
    std::fs::create_dir_all(works_path.parent().unwrap())?;
    let mut works_file = File::create(works_path)?;
    works_file.write_all(serde_json::to_string_pretty(&works_json)?.as_bytes())?;

    Ok(works)
}
